cmake_minimum_required(VERSION 3.17)
project(maq_cpp_example)

set(CMAKE_CXX_STANDARD 17)

set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)

find_package(OpenSSL REQUIRED)

find_package(Protobuf REQUIRED)
include_directories(${PROTOBUF_INCLUDE_DIRS})
include_directories(${CMAKE_CURRENT_BINARY_DIR})
protobuf_generate_cpp(PROTO_SRCS PROTO_HDRS
        proto/account.proto
        proto/auth.proto
        proto/base.proto
        proto/reqrep.proto
        proto/room.proto
        proto/room_list.proto
)

add_executable(maq_cpp_example maq.h main.cpp ${PROTO_SRCS} ${PROTO_HDRS})
target_link_libraries(maq_cpp_example
        PRIVATE Threads::Threads
        ${CMAKE_SOURCE_DIR}/libmaq.a
        ${OPENSSL_LIBRARIES}
        ${PROTOBUF_LIBRARIES}
        ${CMAKE_DL_LIBS}
)